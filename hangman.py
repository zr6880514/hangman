''' Implementation of hangman game. From most common english words: 50 nouns, 20 adjectives,
20 verbs and 10 adverbs game chooses randomly one word to guess'''

import random

nouns = ['time', 'person', 'year', 'way', 'day', 'thing', 'man', 'world', 'life', 'hand', 'part',
    'child', 'eye', 'woman', 'place', 'work', 'week', 'case', 'point', 'government',
    'company', 'number', 'group', 'problem', 'fact', 'be', 'have', 'do', 'say', 'get',
    'make', 'go', 'know', 'take', 'see', 'come', 'think', 'look', 'want', 'give', 'use',
    'find', 'tell', 'ask', 'work', 'seem', 'feel', 'try', 'leave', 'call']

adjectives = ['good', 'new', 'first', 'last', 'long', 'great', 'little', 'own', 'other', 'old',
    'right', 'big', 'high', 'different', 'small', 'large', 'next', 'early', 'young',
    'important']

verbs = ['be', 'have', 'do', 'say', 'get', 'make', 'go', 'know', 'take', 'see', 'come',
    'think', 'look', 'want', 'give', 'use', 'find', 'tell', 'ask', 'work', 'seem',
    'feel', 'try', 'leave', 'call', 'start', 'try', 'need', 'feel', 'become', 'leave',
    'put', 'mean', 'keep', 'let', 'begin', 'seem', 'help', 'talk', 'turn', 'start',
    'show', 'hear', 'play', 'run', 'move', 'like']
adverbs = ['also', 'just', 'not', 'only', 'now', 'here', 'more', 'there', 'still', 'well']

common_words = nouns + adjectives + verbs + adverbs

password = random.choice(common_words).upper()

incorrect_guesses = ""
correct_guesses = ""
guessed_letters = dict()
wrong_guesses_num = 0
max_wrong_guesses = 7

def hangman_drawing(wrong_guesses_num, max_wrong_guesses):
    if wrong_guesses_num == 0:
        print()
    elif wrong_guesses_num == 1:
        print("""   
         ________|
              """)
    elif wrong_guesses_num == 2:
        print("""
         _________
        |/
        |
        |
        |
        |
        |      
        |________|
              """)
    elif wrong_guesses_num == 3:
        print("""
         _________
        |/        |
        |        (_)
        |
        |
        |
        |      
        |________|
              """)
    elif wrong_guesses_num == 4:
        print("""
         _________
        |/        |
        |        (_)
        |        \\|
        |
        |
        |      
        |________|
              """)
    elif wrong_guesses_num == 5:
        print("""
         _________
        |/        |
        |        (_)
        |        \\|/
        |         |
        |
        |      
        |________|
              """)
    elif wrong_guesses_num == 6:
        print("""
         _________
        |/        |
        |        (_)
        |        \\|/
        |         |
        |        /
        |      
        |________|
              """)
    elif wrong_guesses_num == max_wrong_guesses:
        print("""
         _________
        |/        |
        |        (_)
        |        \\|/
        |         |
        |        / \\
        |      
        |________|
              """)

for letter in password:
    guessed_letters[letter] = "hidden"

def password_display(password, guessed_letters):
    hidden_password = ""
    for letter in password:
        if guessed_letters[letter] == "hidden":
            hidden_password += "_ "
        elif guessed_letters[letter] == "unfold":
            hidden_password += f"{letter.upper()} "
    return hidden_password

def is_password_guessed(guessed_letters):
    for letter in guessed_letters:
        if guessed_letters[letter] == "hidden":
            return False
    return True

def getting_character(incorrect_guesses, correct_guesses):
    character = ""
    while len(character) != 1 \
    or character.isalpha() != True \
    or character in (incorrect_guesses + correct_guesses):
        character = input("Guess a letter: ").upper()
        if len(character) != 1:
            print("Must be SINGLE character.")
        elif character.isalpha() != True:
            print("That's not an alphabet letter.")
        elif character in (incorrect_guesses + correct_guesses):
            print("Letter already used.")
    return character
        

while is_password_guessed(guessed_letters) == False:
    print("A word to guess: " + password_display(password, guessed_letters))
        
    user_guess = getting_character(incorrect_guesses, correct_guesses)
          
    if user_guess in password:
        print(f"Great! Letter {user_guess} occurs {password.count(user_guess)} times.")
        guessed_letters[user_guess] = "unfold"
        correct_guesses += user_guess
    else:
        print("Incorrect guess.")
        incorrect_guesses += user_guess + " "
        wrong_guesses_num += 1

    hangman_drawing(wrong_guesses_num, max_wrong_guesses)
    print(f"Incorrect guesses: {incorrect_guesses}")
    print(f"Wrong guesses number: {wrong_guesses_num}.")
    print()

    if wrong_guesses_num == max_wrong_guesses:
        print("Game over, you are out of tries.")
        print(f"Correct password was: {password}")
        break

if is_password_guessed(guessed_letters) == True:
    print(f"Congratulations! You guessed a password: {password}")
